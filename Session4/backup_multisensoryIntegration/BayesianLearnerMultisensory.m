function [out] = BayesianLearnerMultisensory(inp)
% In this script, we're coding a Bayesian learner, that learns the standard
% deviation of the two sources given where the samples from the two sources
% are and where the true location is shown at feedback
% Steps:
% 1) Make a matrix of how close each source's samples were to the true mean
% 2.A) Set up the size of the belief grids (belief in how likely each
% standard deviation is)
% 2.B) Set up flat prior beliefs
% 3) Set up the likelihood, p(distance to true mean|possible STDs) for
% each source 
% 4) Run through all the trials and update the beliefs about the STDs every
% time

% 1) Compute how close each sample of each source was on each trial to the
% true mean
SampleError = inp.schedule.SourceTargError.v; 
ntr = length(SampleError); % number of trials
%keyboard
% 2.A) Set up the size of the belief grids and their labels (i.e. possible
% standard deviations in degree)
% In this experiment - different from the previous learning tasks - each
% outcome (i.e. distance between a sample and the true location) is
% completely independent for the 2 sources, therefore, we can set up 2 1D
% grids, rather than 1 2D grid. This mean faster computation because there
% are fewer entries (i.e. 2*grid length, rather than grid length * grid
% length). 
gridSteps      = inp.stepSize;                            % How large (degr.) steps in the grid (beliefs about STD are)
gridMinMax     = inp.stepMinMax;                          % This is the outer bounds of the possible belief states.
gridLabels     = gridMinMax(1):gridSteps:gridMinMax(2);   % The labels associated with each belief step

% 2.B) Make the belief grid
% Initialize the two 1D belief vectors
BeliefSTD{1} = nan(ntr+1,length(gridLabels)); %ntr+1 because we store the belief after the outcome has been seen, so last entry is posterior after the last trial
BeliefSTD{1}(1,:) = ones(length(gridLabels),1);
% regularize beliefs so that they add up to
BeliefSTD{1}(1,:) = BeliefSTD{1}(1,:)/sum(BeliefSTD{1}(1,:));
BeliefSTD{2} = BeliefSTD{1};


% 3) Set up the matrix to compute the likelihoods for each given outcome
% (p(distance true location to sample|possible standard deviations) 
outcomeStepSize = -180:2:180;
likelihood = nan(length(outcomeStepSize),length(gridLabels));
for ig= 1:length(gridLabels)
    %%%%%%% See Q 4.3.1.1 %%%%%%%
    likelihood(:,ig) = normpdf(outcomeStepSize,0,gridLabels(ig));
    likelihood(:,ig) = likelihood(:,ig)./sum(likelihood(:,ig)); % regularize so that probabilities sum up to 1
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end

% 4) Update beliefs on each trial
% Additional outputs we want to store:
BeliefExpVal      = nan(ntr+1,2); % obtained by integrating over whole belief grid
BeliefPointEstim  = nan(ntr+1,2); % obtained as the peak (i.e. most likely) value of belief grid
for itr=1:ntr % for each trial
    for is = 1:2 % for each source
        prior = BeliefSTD{is}(itr,:);
        
        %%%%%% Q 4.3.1.2 %%%%%%%%
        like = likelihood(outcomeStepSize==SampleError(itr,is),:); % read out the likelihood of the given outcome
        posterior = prior.*like;
        % regularize
        posterior = posterior./sum(posterior);
        %%%%%%%%%%%%%%%%%%%%%%%%%%
        
        
        
        % Prepare the prior for next trial, this means, taking into account that the standard deviation might change between trials
        % the simples implementation of this is to add flat noise
        %%%%%%% Q 4.3.1.3 %%%%%%%%
        posterior = (1-inp.forgetting)*posterior + inp.forgetting*ones(size(posterior))./sum(ones(size(posterior)));
        %%%%%%%%%%%%%%%%%%%%%%%%%%
        
        % Store the regularized posterior;
        BeliefSTD{is}(itr+1,:) = posterior./sum(posterior);
        
        % Also extract the most likely belief, i.e. point estimate
        %%%%% Q4.3.1.4 %%%%% 
        ind =  BeliefSTD{is}(itr+1,:)==max(BeliefSTD{is}(itr+1,:));
        %%%%%%%%%%%%%%%%%%%%
        BeliefPointEstim(itr,is) = gridLabels(ind);
        
        % And extract the best guess belief (i.e. 'expected value'), by
        % integrating
        %%%%% Q4.3.1.5 %%%%%%%%%
        BeliefExpVal(itr,is) = sum(BeliefSTD{is}(itr+1,:).*gridLabels);
        %%%%%%%%%%%%%%%%%%%%%%%%

    end
end
% Store the whole distribution of beliefs
out.belief.distr.v = BeliefSTD;
out.belief.distr.l = 'Each cell: belief of standard deviation of each source on each trial (before the outcome is seen)';
out.belief.distr.labels = gridLabels;
% Store the point estimate (i.e. most likely)
out.belief.pointestim.v = BeliefPointEstim;
out.belief.pointestim.l = 'Point estimate, i.e. most likely std';
% Store the expected STD (i.e. taking into account the whole grid)
out.belief.expval.v = BeliefExpVal;
out.belief.expval.l = 'Expected STD, taking into account whole grid, i.e. uncertainty';
out.inp = inp;
end