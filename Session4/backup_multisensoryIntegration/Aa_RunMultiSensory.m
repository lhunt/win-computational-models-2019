%% This script is running a bayesian model for a simple multi-sensory integration paradigm
% We want to make a Bayesian observer that integrates information from two
% sources (e.g. auditory and visual) to guess the location of a target on a
% circle in a hypothetical experiment.
% Each source is characterized by a 'noisiness' that the model will need to
% take into account when integrating them. Over the course of the
% experiment, the noisiness of each source varies. The model tries to track
% this.

close all
clear
basepath = SetHomePathNew; % outermost folder containing all scripts and the data
%% 4.1.1 Create the experiment
% The script 'CreateMultiSensorySchedule' creates the schedule, i.e. it
% makes a series of trials. On each trial there is a true mean from which
% two sensory sources, with different standard deviations draw a sample
MSched=CreateMultisensorySchedule;
% Questions here! Look at the work sheet (EndQ4.1)
keyboard

%% 4.2.1 Some illustrations of the schedule
% Let's make some illustrations to understand the concepts
% 4.2.1): What does it mean for two sources to draw samples from the same mean,
% but with different standard deviations
% Example Distributions
SamplingResolution=MSched.settings.SamplingResolution;
for es=1:100000
    ExampleDistributionA(es,1)=round(normrnd(0,50./SamplingResolution))*SamplingResolution;
    ExampleDistributionB(es,1)=round(normrnd(0,15./SamplingResolution))*SamplingResolution;
end
figure('color','w');hist([ExampleDistributionA ExampleDistributionB],200);
title('Two example distributions with same mean and different stdevs')

% 4.2.2) What does our schedule look like?
figure('color','w');
subplot(3,2,1); hist(MSched.SourceSamples.v); title('Histogram of samples from the two sources across  experiment');
subplot(3,2,2); hist(MSched.SampleDifference.v); title('Hist. of differences between the two sources across expriment');
% In the next plot note, how because things are arranged in a circle, if
% the true target is at e.g. 5, the average of the two samples could be at e.g. 350
subplot(3,2,3); plot(MSched.TargetPosition.v,MSched.SourceAverage.v,'.'); title('Target location against average of the two sources');xlabel('trials')
subplot(3,2,4); hist(MSched.AverageTargError.v,100); title('Hist. of differences between true location and mean of the two sources');
% Just as an illustration of the noise in each source, let's plot the samples relative to the target location
subplot(3,2,5); plot(MSched.SourceSamples.NonCric(:,1)-MSched.TargetPosition.v); title('Samples relative to target for source 1');xlabel('trials')
subplot(3,2,6); plot(MSched.SourceSamples.NonCric(:,2)-MSched.TargetPosition.v); title('Samples relative to target for source 2');xlabel('trials')

% Questions here! Look at the work sheet (EndQ4.2)
keyboard;

%% 4.3.1 Bayesian model to estimate the standard deviations (Bayesian observer)
% Settings for the Bayesian learner
inp.forgetting    = 0.01;         % The model has some forgetting so that the Bayesian model doesn't get stuck with longer experiments. A more sophisticated model could be built that tries to learn also how likely standard deviations are to change (but given that there are only 2 changes per option, this would be hard for the model)
inp.stepSize      = 4;            % Making the belief grid, how closely it is spaced for estimating standard deviations
inp.stepMinMax    = [2 60];       % The range of the STD grid
inp.schedule      = MSched;

% Bayesian learner that updates beliefs about the standard deviations of the two distributions, given the samples on each trial and the true location revealed at feedback
outLearn=BayesianLearnerMultisensory(inp); %<==== Go in here!

% Let's plot the outputs of the Bayesian learner
% 4.3.1) Belief distributions on each trial
figure('color','w');
for ip =1:2
    subplot(2,1,ip);
    imagesc(outLearn.belief.distr.v{ip});
    set(gca,'XTick',1:1:length(outLearn.belief.distr.labels),'XTickLabel',outLearn.belief.distr.labels);xlabel('STD');ylabel('Trial');c=colorbar;c.Label.String='BeliefStrength';
    title(['Source ' num2str(ip)]);
end

% 4.3.2) Best guess of STD (i.e. 'expected value') and most likely  STD on each trial
figure('color','w')
for ip=1:2
    subplot(2,1,ip);plot(outLearn.inp.schedule.TrueSTD.v(:,ip),'k');hold on
    plot(outLearn.belief.expval.v(:,ip),'b')
    plot(outLearn.belief.pointestim.v(:,ip),'r')
    title(['Source' num2str(ip)]);legend('TrueSTD','Expected value','Point estimate')
    set(gca,'FontSize',12)
end
% Questions here! Look at the work sheet (EndQ4.3)
keyboard;
%%  4.4.1 Bayesian model to integrate the two sources for predictions

outLearn.forgetting    =0.01;                   % The model has some forgetting so that the Bayesian model doesn't get stuck with longer experiments.
outLearn.MeanDifference=MSched.SampleDifference.v;
outLearn.MeanAverage   =MSched.SourceAverage.v   ;
outLearn.stepSize      =4;                      % The sampling of possible STD's grid
outLearn.stepMinMax    =[2 60];                 % The range of the STD grid
outLearn.outc          =MSched.AverageTargError.v;
outLearn.samples       =MSched.SourceSamples.v;
outLearn.TrueSTD       =MSched.TrueSTD.v;
outLearn.MeanError     =MSched.AverageTargError.v;
outLearn.BlockID       =MSched.BlockID.v;

outDec=BayesianDeciderMultisensory(outLearn); %<==== Go in here!
% Questions here! Look at the work sheet (EndQ4.4)
keyboard;