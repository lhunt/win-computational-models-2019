function [out] = BayesianObserverMultiDistributions(inp)
% This Bayesian model will do the following steps:
% 1) Set up the grid for p(beliefs), which representes here the belief in
% STD for each source, i.e. a 2D grid
% 2) Precompute p(outcome|possible beliefs), where the outcome is the difference
% between the true position (revealed as feedback) and the mid-point
% between the two samples
% 3) Use 1) and 2) to run across all trials, each time updating beliefs
% given the actual outcomes and the probability of the outcomes (given
% possible beliefs)
% 4) To allow the model to adjust when the standard deviation has changed,
% include some 'forgetting' or 'leakage' between trials

keyboard

% Extract data from the schedule and relabel to have better access
ntr = length(inp.schedule.SourceSamples);
outc= inp.outc;
samples=inp.samples; %inp.SourceSamples
sampleDifference=inp.MeanDifference;
TrueSTD=inp.TrueSTD;
f=inp.forgetting;

% Set up the grids used for bayesian inference. Here we will have a 2D grid
% of Stdev source A x STdev source B. Each entry is the probability of a
% possible combination of the standard deviations (with the labels of each
% cell saying what the standard deviations are)
gridSteps      = inp.stepSize;                               % We want to express our beliefs as a distribution with a step/gridsize of x
gridMinMax     = inp.stepMinMax;                             % This is the outer bounds of the possible belief states.
gridLabels     = gridMinMax(1):gridSteps:gridMinMax(2);      % The associated with each belief step
BeliefMatrix(1,:,:) = ...
repmat(1./((length(gridLabels)*length(gridLabels))),length(gridLabels),length(gridLabels));  % initialize the beliefs - in the beginning we know nothing, so each belief is equally likely, and all beliefs need to add up to 100%

OutcomeGridMinMax     =[-360 360];             % As outcomes run on a circle,  outcomes regarding the distribution don't need to go beyond 360
shortOutcomeGridMinMax=[-180 180];             % In fact, as it is relative to the mean between both samples, values only need to go between -180 and +180
OutcomeGridStepsize   =2;SampleGridStepsize=4; % As with the actual Sampling, the model has a resolution of True positions of 2 and 4 for the sampling from it.

OutcomeGridLabels     =OutcomeGridMinMax(1):(OutcomeGridStepsize):OutcomeGridMinMax(2);
shortOutcomeGridLabels=shortOutcomeGridMinMax(1):OutcomeGridStepsize:shortOutcomeGridMinMax(2);    % determined by the sampling resolution

MaxMinSampleDifference=[-180 180];             % The difference between samples can of course also not exceed -180 +180                 
gridSampleDifferences =MaxMinSampleDifference(1):(SampleGridStepsize):MaxMinSampleDifference(2); % The Difference can only be twice as much as sampling itself

%% Precompute Observation Probability Matrix
%First we need to pre-compute the probability of a specific set of Samples Sample 1 and 2, with all possible combinations of standard deviations
%and true means of distribution. Only after we update probabilities of specific configurations, particularly differing Standard deviations.

% This grid will give us the probability of all true mean values relative to the mean of both samples, as a function of sample distances from each other
% and their respective standard deviations
OutcomeProbability=nan(length(shortOutcomeGridLabels),length(gridSampleDifferences),length(gridLabels),length(gridLabels));

% But first we generate the likelihood function, i.e. p(data|parameters).
% This means, for each combination of standard deviations of the two
% sources deriving a probability of observing each possible outcome (i.e. 


%Probability density distribution for normal
% distributions with specific distances from mean between samples anddondo
% different standard deviations. We only need to do this once because it is
% the same for sample one and two.
for s=1:length(gridLabels)
    for m=1:length(shortOutcomeGridLabels)
        % This is the distribution of probabilities if the true mean is at m (relative to mean between samples) and standard deviation is s
        TrueMeanProbDistribution(:,m,s)=...
            normpdf(OutcomeGridLabels,shortOutcomeGridLabels(m),gridLabels(s));
    end
end

% Now we have to make sure the fact that angles wrap around is taken into
% account. In other words, -360, 360 and 0 Are the same. So is -181 and 179 for the samples around the mean
for g=1:length(shortOutcomeGridLabels)
    shortTrueMeanProbDistribution(g,:,:)=TrueMeanProbDistribution(OutcomeGridLabels==shortOutcomeGridLabels(g),:,:);
    if shortOutcomeGridLabels(g)==0
        shortTrueMeanProbDistribution( g,:,:)= shortTrueMeanProbDistribution( g,:,:)+  TrueMeanProbDistribution( OutcomeGridLabels==(shortOutcomeGridLabels(g)+360),:,:);
        shortTrueMeanProbDistribution( g,:,:)= shortTrueMeanProbDistribution( g,:,:)+  TrueMeanProbDistribution( OutcomeGridLabels==(shortOutcomeGridLabels(g)-360),:,:);
    elseif (shortOutcomeGridLabels(g)+360)<360
        shortTrueMeanProbDistribution( g,:,:)= shortTrueMeanProbDistribution( g,:,:)+  TrueMeanProbDistribution( OutcomeGridLabels==(shortOutcomeGridLabels(g)+360),:,:);
    elseif (shortOutcomeGridLabels(g)-360)>-360
        shortTrueMeanProbDistribution( g,:,:)= shortTrueMeanProbDistribution( g,:,:)+  TrueMeanProbDistribution( OutcomeGridLabels==(shortOutcomeGridLabels(g)-360),:,:);
    end
end

% Now we compute the Outcome Probability Matrix. This determines how likely
% observations are with A specific true mean, sampling differences and
% different varibility levels of samples.
for s1=1:length(gridLabels)
    for s2=1:length(gridLabels)
        for m=1:length(shortOutcomeGridLabels)
            for d=1:length(gridSampleDifferences)
                Sample1Location=-gridSampleDifferences(d)./2;
                Sample2Location= gridSampleDifferences(d)./2;
                % Here is the Multiplication of both Probability Distributions for joint probability of a certain mean at
                % m with SampleDifference d and standard deviations of s1 and s2
                OutcomeProbability(m,d,s1,s2)= shortTrueMeanProbDistribution(shortOutcomeGridLabels==Sample1Location,m,s1).*...
                    shortTrueMeanProbDistribution(shortOutcomeGridLabels==Sample2Location,m,s2);
            end
        end
    end
end

% Now regularize
OutcomeProbability=OutcomeProbability./sum(OutcomeProbability(:));


% Here some useful Plots regarding the probability space created
figure;plot(OutcomeGridLabels,TrueMeanProbDistribution(:,:,4))
title('Probability distributions with all means and STD 40 non-wrapped');

figure;plot(shortOutcomeGridLabels,shortTrueMeanProbDistribution(:,:,4))
title('Probability distributions with all means and STD 40 wrapped around');

figure;radarPlot(shortTrueMeanProbDistribution(:,:,4))
title('Probability distributions with all means and STD 40 wrapped around (Radar)');

figure;plot(shortOutcomeGridLabels,OutcomeProbability(:,gridSampleDifferences==60,2,6))
title(['Probability Distribution of True mean with 120 Difference between samples and assumed STD1:' num2str(gridLabels(2))  ', STD2:' num2str(gridLabels(6))]);

figure;plot(gridSampleDifferences,squeeze(OutcomeProbability(shortOutcomeGridLabels==0,:,2,6)))
title(['Probability of Sample differences with True mean on the difference and STD1:' num2str(gridLabels(2)) ', STD2:' num2str(gridLabels(6))]);

figure;plot(gridLabels,squeeze(OutcomeProbability(shortOutcomeGridLabels==0,gridSampleDifferences==60,:,1)))
title(['Probability of STD1s with True mean at 0, Sample difference of 124, STD2:' num2str(gridLabels(1))  ]);

figure;subplot(2,1,1);plot(gridLabels,squeeze(sum(OutcomeProbability(shortOutcomeGridLabels==40,gridSampleDifferences==60,:,:),4)))
subplot(2,1,2);plot(gridLabels,squeeze(sum(OutcomeProbability(shortOutcomeGridLabels==40,gridSampleDifferences==60,:,:),3)))
title('Probability of STD1s and STD2s with True mean at 40, Sample difference of 60');

figure;subplot(2,1,1);plot(gridLabels,squeeze(sum(OutcomeProbability(:,gridSampleDifferences==-20,:,:),4)))
subplot(2,1,2);plot(gridLabels,squeeze(sum(OutcomeProbability(:,gridSampleDifferences==-20,:,:),3)))
title('Probability of STD1s and STD2s with all True means, Sample difference of -20');
legend(num2str(shortOutcomeGridLabels'))

figure;subplot(2,1,1);plot(gridLabels,squeeze(OutcomeProbability(shortOutcomeGridLabels==40,gridSampleDifferences==60,:,1)))
subplot(2,1,2);plot(gridLabels,squeeze(OutcomeProbability(shortOutcomeGridLabels==40,gridSampleDifferences==60,1,:)))
title('Probability of STD1s and STD2s with True mean at 40, Sample difference of 60');

figure;subplot(2,1,1);plot(gridLabels,squeeze(OutcomeProbability(shortOutcomeGridLabels==-40,gridSampleDifferences==60,:,1)))
subplot(2,1,2);plot(gridLabels,squeeze(OutcomeProbability(shortOutcomeGridLabels==-40,gridSampleDifferences==60,1,:)))
title('Probability of STD1s and STD2s with True mean at -40, Sample difference of 60');


%keyboard;
% Now the OutcomeProbability grid got all the info necessary,
% i.e.Probabilities of for true means m having a Distance from Mean of samples of
% shortOutcomeGridLabels, distance between samples of d
% gridSampleDifferences with S1 being negative and S2 positive, and standard deviations of Sample 1 s1 and
% Sample 2 s2.

% Update our beliefs, based on the outcomes we observe on each trial
% Each time, apply Bayes' rule to compute the posterior

% Remember how we intialized beliefs with BeliefMatrix(1,:,:) = repmat(1./((size(BeliefMatrix,2)*size(BeliefMatrix,3)),size(BeliefMatrix,2),size(BeliefMatrix,3));
FlatPrior = repmat(1./((size(BeliefMatrix,2)*size(BeliefMatrix,3))),size(BeliefMatrix,2),size(BeliefMatrix,3));
%SimpleFlatPrior1=repmat(1./(size(BeliefMatrix,2)),size(BeliefMatrix,2),1);    SimpleFlatPrior2=repmat(1./(size(BeliefMatrix,3)),size(BeliefMatrix,3),1);
%STD1Belief=repmat(1./(size(BeliefMatrix,2)),1,size(BeliefMatrix,2));
%STD2Belief=repmat(1./(size(BeliefMatrix,3)),1,size(BeliefMatrix,3));
%OutcomeDistributionProbs(:,:,:,:,1)=OutcomeProbability;
for itr=1:ntr
    % Step 1: apply Bayes' rule (without regularization) to compute the
    % posterior for each belief, i.e. posterior = prior * likelihood of the observed
    % outcome    
    
    % Probability of the STD's given the trial, Dependent on True Mean and dependent on Sample Difference
    TrialOberservationSTDProbs=squeeze((OutcomeProbability(shortOutcomeGridLabels==outc(itr),sampleDifference(itr)==gridSampleDifferences,:,:)));
    TrialOberservationSTDProbs=TrialOberservationSTDProbs/sum(TrialOberservationSTDProbs(:));   % Marginalize
    posterior = squeeze(BeliefMatrix(itr,:,:)).*TrialOberservationSTDProbs;
    
    %STD1Probs=sum(TrialOberservationSTDProbs,2);
    %STD2Probs=sum(TrialOberservationSTDProbs,1)';
    
    %AllSTDProbs(itr,:,1)=STD1Probs;    AllSTDProbs(itr,:,2)=STD2Probs;
    
    % Step 2: apply regularization to make sure all the beliefs add up to
    % 100% again
    posterior = posterior./sum(posterior(:));
    % step 3: the posterior on one trial is the prior on the next trial
    % plus some forgetting
    BeliefMatrix(itr+1,:,:) = posterior*(1-f) + FlatPrior*f;
    
    %{
    Posterior1=((STD1Belief(itr,:)').*STD1Probs);
    Posterior2=((STD2Belief(itr,:)').*STD2Probs);    
    Posterior1=Posterior1./sum(Posterior1(:));
    Posterior2=Posterior2./sum(Posterior2(:));
    
    STD1Belief(itr+1,:)=Posterior1*(1-f) + SimpleFlatPrior1*f;
    STD2Belief(itr+1,:)=Posterior2*(1-f) + SimpleFlatPrior2*f;     
    %}
    for out=1:size(OutcomeProbability,1)
       for dif=1:size(OutcomeProbability,2) 
           % Take the same belief for outcome and Difference, but use weighted matrix for STD's. 
           % This means people only learn about STD's not basic location or difference from mean        
           FullBeliefMatrix(out,dif,:,:)=BeliefMatrix(itr,:,:);        
       end
    end
    % regularize
    FullBeliefMatrix=FullBeliefMatrix./sum(FullBeliefMatrix(:));
    
    
    % Generate a probability matrix with estimates for locations and
    % sampling differences
    OutcomeDistributionProbs       =  OutcomeProbability.*FullBeliefMatrix;
    OutcomeDistributionProbsPostSample =  squeeze(OutcomeDistributionProbs(:,sampleDifference(itr)==gridSampleDifferences,:,:));
    OutcomeDistributionProbsPostSample=OutcomeDistributionProbsPostSample ./sum(OutcomeDistributionProbsPostSample(:));
    
    % Compute some useful distributions through marginalization of all other dimensions    
    SampleAndPositionEstimates(:,:,itr)=squeeze(sum(sum(OutcomeProbability.*FullBeliefMatrix,3),4));
    TruePositionEstimates(:,itr)=squeeze(sum(sum(OutcomeDistributionProbsPostSample,3),2));
    SampleEstimates(:,itr)=squeeze(sum(sum(sum(OutcomeDistributionProbs,3),4),1));
    TruePositionBestGuess(itr,:)=shortOutcomeGridLabels(find(TruePositionEstimates(:,itr)==max(TruePositionEstimates(:,itr)),1,'first'));

    
end
%keyboard;
% Rather than using the best guess, i.e. max probability value as our
% current estimate, we can do something a bit more sophisticated by
% integrating across the whole belief distribution. Whether this is better
% or not depends on the structure of the world. e.g. for a bimodal
% distribution, the integral could be exactly the wrong thing to do.
TruePositionIntegral=sum(TruePositionEstimates.*repmat(shortOutcomeGridLabels',1,size(TruePositionEstimates,2)),1);

% Computes the relative weighting of both sources of evidence by computing
% how far from the equal weighting i.e. prediction exactly in the middle
% between samples the bayesian model converges. Of course the distance from
% the mean needs to be made relative to the half distance between samples as
% that is the maximal weighting of one over the other. 
RelativeEvidenceWeightingBG=TruePositionBestGuess./sampleDifference/2;
RelativeEvidenceWeightingIntegral=TruePositionIntegral'./sampleDifference/2;


% Some more useful figures showing the relative weighting across trials 
figure;subplot(2,1,1);plot(RelativeEvidenceWeightingBG);subplot(2,1,2);plot(TrueSTD );
title('Relative weighting of both sources [Expectation/HalfDistance] using best guess');
figure;subplot(2,1,1);plot(RelativeEvidenceWeightingIntegral);subplot(2,1,2);plot(TrueSTD );
title('Relative weighting of both sources [Expectation/HalfDistance] using integral');

% These figures show how good the Bayesian model is compared to a simple
% average between the samples
figure;subplot(3,1,1);hist(inp.MeanError,60);xlim([-60 60])
title('Mean Error using half distance between samples');
subplot(3,1,2);hist(outc-TruePositionBestGuess,60);xlim([-60 60])
title('Mean error using Bayesian model and Best Guess');
subplot(3,1,3);hist(outc-TruePositionIntegral',60);xlim([-60 60])
title('Mean error using Bayesian model and integral');


% This shows the Bayesian predictions vs true outcomes for each block
% separately. It shows the model can follow the changes in errors by
% tracking the changing STD's
figure;
for b=1:max(inp.BlockID)  
    cIndex= (inp.BlockID'==b) & (sampleDifference~=0);
    subplot(max(inp.BlockID),1,b);
    hist([TruePositionIntegral(cIndex)' outc(cIndex)].*(1-2.*(sampleDifference(cIndex)<0)),60);xlim([-80 80])
    title(['Block:' num2str(b) '  True Position estimate and actual outcomes']);
    legend({'Estimates of True position';'True position'})
end

% These figures show the STD believes across the experiment for both option
% A and B.
figure;subplot(5,1,1:4);plot(sum(BeliefMatrix,3))
legend(num2str(gridLabels'))
xlim([1 size(BeliefMatrix,1)]);
title('Estimates of STD A')
subplot(5,1,5);plot(TrueSTD(:,1))
ylim(gridMinMax)
figure;subplot(5,1,1:4);plot(squeeze(sum(BeliefMatrix,2)))
legend(num2str(gridLabels'))
xlim([1 size(BeliefMatrix,1)]);
title('Estimates of STD B')
subplot(5,1,5);plot(TrueSTD(:,2))
ylim(gridMinMax)


% Do we still need this? It isn't quite the correct thing, but maybe
% simpler?
% figure;subplot(5,1,1:4);plot(STD1Belief)
% legend(num2str(gridLabels'))
% xlim([1 size(BeliefMatrix,1)]);
% title('Estimates of STD A separate')
% subplot(5,1,5);plot(TrueSTD(:,1))
% ylim(gridMinMax)
% figure;subplot(5,1,1:4);plot(STD2Belief)
% legend(num2str(gridLabels'))
% xlim([1 size(BeliefMatrix,1)]);
% title('Estimates of STD B separate')
% subplot(5,1,5);plot(TrueSTD(:,2))
% ylim(gridMinMax)

keyboard;

% Store outputs  [Nils: What outputs do we need?]
out.x.l = 'Probability predictions, full grid';
out.x.v = probs;
out.ModelDescr = 'A model trying to estimate the most likely location of an object in space, from both visual and auditory information with different variability of samples';