function [out] = BayesianDeciderMultisensory(inp)


% Extract data from the schedule and relabel to have better access
ntr = length(inp.samples);
outc= inp.outc;
samples=inp.samples; %inp.SourceSamples
sampleDifference=inp.MeanDifference;
TrueSTD=inp.TrueSTD;

% Set up the grids used for bayesian inference. Here we will have a 2D grid
% of Stdev source A x STdev source B. Each entry is the probability of a
% possible combination of the standard deviations (with the labels of each
% cell saying what the standard deviations are)
gridSteps      = inp.stepSize;                               % We want to express our beliefs as a distribution with a step/gridsize of x
gridMinMax     = inp.stepMinMax;                             % This is the outer bounds of the possible belief states.
gridLabels     = gridMinMax(1):gridSteps:gridMinMax(2);      % The associated with each belief step
BeliefMatrix(1,:,:) = ...
    repmat(1./((length(gridLabels)*length(gridLabels))),length(gridLabels),length(gridLabels));  % initialize the beliefs - in the beginning we know nothing, so each belief is equally likely, and all beliefs need to add up to 100%

OutcomeGridMinMax     =[-360 360];             % As outcomes run on a circle,  outcomes regarding the distribution don't need to go beyond 360
shortOutcomeGridMinMax=[-180 180];             % In fact, as it is relative to the mean between both samples, values only need to go between -180 and +180
OutcomeGridStepsize   =2;SampleGridStepsize=4; % As with the actual Sampling, the model has a resolution of True positions of 2 and 4 for the sampling from it.

OutcomeGridLabels     =OutcomeGridMinMax(1):(OutcomeGridStepsize):OutcomeGridMinMax(2);
shortOutcomeGridLabels=shortOutcomeGridMinMax(1):OutcomeGridStepsize:shortOutcomeGridMinMax(2);    % determined by the sampling resolution

MaxMinSampleDifference=[-180 180];             % The difference between samples can of course also not exceed -180 +180
gridSampleDifferences =MaxMinSampleDifference(1):(SampleGridStepsize):MaxMinSampleDifference(2); % The Difference can only be twice as much as sampling itself


%% Precompute Observation Probability Matrix
%First we need to pre-compute the probability of a specific set of Samples Sample 1 and 2, with all possible combinations of standard deviations
%and true means of distribution. Only after we update probabilities of specific configurations, particularly differing Standard deviations.

% This grid will give us the probability of all true mean values relative to the mean of both samples, as a function of sample distances from each other
% and their respective standard deviations
OutcomeProbability=nan(length(shortOutcomeGridLabels),length(gridSampleDifferences),length(gridLabels),length(gridLabels));

% But first we generate the likelihood function, i.e. p(data|parameters).
% This means, for each combination of standard deviations of the two
% sources deriving a probability of observing each possible outcome (i.e.


%Probability density distribution for normal
% distributions with specific distances from mean between samples anddondo
% different standard deviations. We only need to do this once because it is
% the same for sample one and two.
for s=1:length(gridLabels)
    for m=1:length(shortOutcomeGridLabels)
        % This is the distribution of probabilities if the true mean is at m (relative to mean between samples) and standard deviation is s
        TrueMeanProbDistribution(:,m,s)=...
            normpdf(OutcomeGridLabels,shortOutcomeGridLabels(m),gridLabels(s));
    end
end

% Now we have to make sure the fact that angles wrap around is taken into
% account. In other words, -360, 360 and 0 Are the same. So is -181 and 179 for the samples around the mean
for g=1:length(shortOutcomeGridLabels)
    shortTrueMeanProbDistribution(g,:,:)=TrueMeanProbDistribution(OutcomeGridLabels==shortOutcomeGridLabels(g),:,:);
    if shortOutcomeGridLabels(g)==0
        shortTrueMeanProbDistribution( g,:,:)= shortTrueMeanProbDistribution( g,:,:)+  TrueMeanProbDistribution( OutcomeGridLabels==(shortOutcomeGridLabels(g)+360),:,:);
        shortTrueMeanProbDistribution( g,:,:)= shortTrueMeanProbDistribution( g,:,:)+  TrueMeanProbDistribution( OutcomeGridLabels==(shortOutcomeGridLabels(g)-360),:,:);
    elseif (shortOutcomeGridLabels(g)+360)<360
        shortTrueMeanProbDistribution( g,:,:)= shortTrueMeanProbDistribution( g,:,:)+  TrueMeanProbDistribution( OutcomeGridLabels==(shortOutcomeGridLabels(g)+360),:,:);
    elseif (shortOutcomeGridLabels(g)-360)>-360
        shortTrueMeanProbDistribution( g,:,:)= shortTrueMeanProbDistribution( g,:,:)+  TrueMeanProbDistribution( OutcomeGridLabels==(shortOutcomeGridLabels(g)-360),:,:);
    end
end

% Now we compute the Outcome Probability Matrix. This determines how likely
% observations are with A specific true mean, sampling differences and
% different varibility levels of samples.
for s1=1:length(gridLabels)
    for s2=1:length(gridLabels)
        for m=1:length(shortOutcomeGridLabels)
            for d=1:length(gridSampleDifferences)
                Sample1Location=-gridSampleDifferences(d)./2;
                Sample2Location= gridSampleDifferences(d)./2;
                % Here is the Multiplication of both Probability Distributions for joint probability of a certain mean at
                % m with SampleDifference d and standard deviations of s1 and s2
                OutcomeProbability(m,d,s1,s2)= shortTrueMeanProbDistribution(shortOutcomeGridLabels==Sample1Location,m,s1).*...
                    shortTrueMeanProbDistribution(shortOutcomeGridLabels==Sample2Location,m,s2);
            end
        end
    end
end

% Now regularize
OutcomeProbability=OutcomeProbability./sum(OutcomeProbability(:));

% Now integrate Learned estimates of Standard deviations with Belief matrix.
STD1=inp.belief.distr.v{1};STD2=inp.belief.distr.v{2};

for itr=1:length(sampleDifference)
    % Lets read out the probability of true positions and standard deviations
    % from the current Trials observation only
    OutcomeProbsPostSample =  squeeze(OutcomeProbability(:,sampleDifference(itr)==gridSampleDifferences,:,:));
    OutcomeProbsPostSample =  OutcomeProbsPostSample./sum(OutcomeProbsPostSample(:));
    
    
    % And now we can combine it with the Learnt information but for that, we need to expand the probability of STD's here to make predictions about
    % true outcomes using all possible combinations of STD's with the given
    % sample and possible true positions
    %keyboard;
    
    %OutcomeProbsPostSample
    
    STD1expanded=repmat(STD1(itr,:)',1,length(STD2(itr,:)));
    STD2expanded=repmat(STD2(itr,:),length(STD1(itr,:)),1);
    STDsProbs=STD1expanded.*STD2expanded;
    OutcomeProbsPostSampleAndLearning=nan(size(OutcomeProbsPostSample));
    for m=1:size(OutcomeProbsPostSample,1)
        OutcomeProbsPostSampleAndLearning(m,:,:)=squeeze(OutcomeProbsPostSample(m,:,:)).*STDsProbs;
    end
    
    % Compute some useful Information through marginalization of all other dimensions
    TruePositionEstimates(:,itr)=squeeze(sum(sum(OutcomeProbsPostSampleAndLearning,3),2));
    TruePositionEstimates(:,itr)=TruePositionEstimates(:,itr)./sum(TruePositionEstimates(:,itr));
    TruePositionBestGuess(itr,1)=shortOutcomeGridLabels(find(TruePositionEstimates(:,itr)==max(TruePositionEstimates(:,itr)),1,'first'));
    % Rather than using the best guess, i.e. max probability value as our
    % current estimate, we can do something a bit more sophisticated by
    % integrating across the whole belief distribution. Whether this is better
    % or not depends on the structure of the world. e.g. for a bimodal
    % distribution, the integral could be exactly the wrong thing to do.
    TruePositionIntegral(itr,1)=sum(TruePositionEstimates(:,itr).*shortOutcomeGridLabels',1);
    
end

%keyboard;

% Here some useful Plots regarding the probability space created
figure;plot(OutcomeGridLabels,TrueMeanProbDistribution(:,:,4))
title('Probability distributions with all means and STD 40 non-wrapped');

figure;plot(shortOutcomeGridLabels,shortTrueMeanProbDistribution(:,:,4))
title('Probability distributions with all means and STD 40 wrapped around');

figure;plot(shortOutcomeGridLabels,OutcomeProbability(:,gridSampleDifferences==60,2,6))
title(['Probability Distribution of True mean with 120 Difference between samples and assumed STD1:' num2str(gridLabels(2))  ', STD2:' num2str(gridLabels(6))]);

figure;plot(gridSampleDifferences,squeeze(OutcomeProbability(shortOutcomeGridLabels==0,:,2,6)))
title(['Probability of Sample differences with True mean on the difference and STD1:' num2str(gridLabels(2)) ', STD2:' num2str(gridLabels(6))]);

figure;plot(gridLabels,squeeze(OutcomeProbability(shortOutcomeGridLabels==0,gridSampleDifferences==60,:,1)))
title(['Probability of STD1s with True mean at 0, Sample difference of 124, STD2:' num2str(gridLabels(1))  ]);

figure;subplot(2,1,1);plot(gridLabels,squeeze(sum(OutcomeProbability(shortOutcomeGridLabels==40,gridSampleDifferences==60,:,:),4)))
subplot(2,1,2);plot(gridLabels,squeeze(sum(OutcomeProbability(shortOutcomeGridLabels==40,gridSampleDifferences==60,:,:),3)))
title('Probability of STD1s and STD2s with True mean at 40, Sample difference of 60');

figure;subplot(2,1,1);plot(gridLabels,squeeze(sum(OutcomeProbability(:,gridSampleDifferences==-20,:,:),4)))
subplot(2,1,2);plot(gridLabels,squeeze(sum(OutcomeProbability(:,gridSampleDifferences==-20,:,:),3)))
title('Probability of STD1s and STD2s with all True means, Sample difference of -20');
legend(num2str(shortOutcomeGridLabels'))

figure;subplot(2,1,1);plot(gridLabels,squeeze(OutcomeProbability(shortOutcomeGridLabels==40,gridSampleDifferences==60,:,1)))
subplot(2,1,2);plot(gridLabels,squeeze(OutcomeProbability(shortOutcomeGridLabels==40,gridSampleDifferences==60,1,:)))
title('Probability of STD1s and STD2s with True mean at 40, Sample difference of 60');

figure;subplot(2,1,1);plot(gridLabels,squeeze(OutcomeProbability(shortOutcomeGridLabels==-40,gridSampleDifferences==60,:,1)))
subplot(2,1,2);plot(gridLabels,squeeze(OutcomeProbability(shortOutcomeGridLabels==-40,gridSampleDifferences==60,1,:)))
title('Probability of STD1s and STD2s with True mean at -40, Sample difference of 60');

% Computes the relative weighting of both sources of evidence by computing
% how far from the equal weighting i.e. prediction exactly in the middle
% between samples the bayesian model converges. Of course the distance from
% the mean needs to be made relative to the half distance between samples as
% that is the maximal weighting of one over the other.
RelativeEvidenceWeightingBG=TruePositionBestGuess./sampleDifference/2;
RelativeEvidenceWeightingIntegral=TruePositionIntegral./sampleDifference/2;


% Some more useful figures showing the relative weighting across trials
figure;subplot(2,1,1);plot(RelativeEvidenceWeightingBG);subplot(2,1,2);plot(TrueSTD );
title('Relative weighting of both sources [Expectation/HalfDistance] using best guess');
figure;subplot(2,1,1);plot(RelativeEvidenceWeightingIntegral);subplot(2,1,2);plot(TrueSTD );
title('Relative weighting of both sources [Expectation/HalfDistance] using integral');

% These figures show how good the Bayesian model is compared to a simple
% average between the samples
figure;subplot(3,1,1);hist(inp.MeanError,60);xlim([-60 60])
title('Mean Error using half distance between samples');
subplot(3,1,2);hist(outc-TruePositionBestGuess,60);xlim([-60 60])
title('Mean error using Bayesian model and Best Guess');
subplot(3,1,3);hist(outc-TruePositionIntegral,60);xlim([-60 60])
title('Mean error using Bayesian model and integral');


% This shows the Bayesian predictions vs true outcomes for each block
% separately. It shows the model can follow the changes in errors by
% tracking the changing STD's
figure;
for b=1:max(inp.BlockID)
    cIndex= (inp.BlockID==b) & (sampleDifference~=0);
    subplot(max(inp.BlockID),1,b);
    %hist([TruePositionIntegral(cIndex) outc(cIndex)].*(1-2.*(sampleDifference(cIndex)<0)),60);xlim([-80 80])
    hist([TruePositionIntegral(cIndex) outc(cIndex)].*repmat(1-2.*(sampleDifference(cIndex)<0),1,2),60);xlim([-80 80])
    title(['Block:' num2str(b) '  True Position estimate and actual outcomes']);
    legend({'Estimates of True position';'True position'})
end


out.RelativeEvidenceWeightingBG=RelativeEvidenceWeightingBG;
out.RelativeEvidenceWeightingIntegral=RelativeEvidenceWeightingIntegral;
out.TruePositionEstimates=TruePositionEstimates;
out.TruePositionBestGuess=TruePositionBestGuess;
out.TruePositionIntegral =TruePositionIntegral;
