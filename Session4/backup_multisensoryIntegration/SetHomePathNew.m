function homePath = SetHomePathNew()
% Change the path here to be the parent folder with all the different
% subfolders in it
% Then copy these two lines to command window and run them so that the path
% is recognized
% From then on, path setting should work automatically
homePath = pwd; 
addpath(genpath(homePath))