function [outp] = CreateMultisensorySchedule()
% This script does the following:
% 1) Set up a list of true positions of the target for each trial (randomly
% placed on a circle)
% 2) Make a list of how noisy each source is on a trial, given that each
% changes of the course of the experiment
% 3) Given the noisiness of each source, generate an observation from each
% source for each trial using sample ~ normal(TruePosition,SD_source). In
% other words, the observation that each source provides is unbiased, i.e.
% centered on the true mean, but it is noisy
% 4) Store the values for later processing with the Bayesian learner

% 1) Draw the true positions of the target 
NrTrials=300;                                            % Number of trials overall
TruePositionResolution=2;                                % How close samples can be to each other (in degrees)
MinmaxTruePosition    =[0 360]./TruePositionResolution;  % 
% Generate random positions for the true location of the source on each
% trial:
TruePosition          =(MinmaxTruePosition(1)+randi(MinmaxTruePosition(2)-MinmaxTruePosition(1)+1,NrTrials,1)-1)*TruePositionResolution;   % Randomly generated true position with a flat prior.


% 2)+3) Set up how standard deviations for each source will change over the
% course of the experiment
% Standard deviations of the two sources (e.g. sound and visual)
SensoryA_STD=[30 4 30];                     % Lvl's of standard deviation for sensory source A
SensoryB_STD=[8 30 30];                     % Lvl's of standard deviation for sensory source B
NumberOfChanges = length(SensoryA_STD)-1;   % This is the number of changes in STD for each source in the task              
BlockLength=round(NrTrials/(1+NumberOfChanges));
SamplingResolution=4;                       % spacing of the samples

% Pre-allocate the containers for the samples from each source
SourceSamples = nan(NrTrials,2); % Samples for each source
TrueSTD         = nan(NrTrials,2); % record of the true standard deviation on that trial for each source
BlockID         = nan(NrTrials,1); % keeping track of which block we're in

ctr=0; % current trial
for iblock = 1:(NumberOfChanges+1)
    for itrblock = 1:BlockLength        
        ctr=ctr+1;
        % 2) Note down noisiness (stdev on each trial)
        TrueSTD(ctr,:) = [SensoryA_STD(iblock) SensoryB_STD(iblock)];
        BlockID(ctr)   = iblock;
        % Given the standard deviations and the true location of the target, draw samples from the two distributions
        % 3) Given the noisiness, draw a random sample on each trial for each source        
        SourceSamples(ctr,1) = round(normrnd(TruePosition(ctr)/SamplingResolution,TrueSTD(ctr,1)/SamplingResolution))*SamplingResolution;
        SourceSamples(ctr,2) = round(normrnd(TruePosition(ctr)/SamplingResolution,TrueSTD(ctr,2)/SamplingResolution))*SamplingResolution;
    end
end

% Because we are on a circle, values over 360 degree and below 0 need to be adjusted
% (for plotting, let's also store these samples before correcting for the circle):
SourceSamplesNonCirc = SourceSamples;
SourceSamples(SourceSamples>360)=SourceSamples(SourceSamples>360)-360; 
SourceSamples(SourceSamples<0)  =SourceSamples(SourceSamples<0)+360;   

% Now we have the samples on each trial from the two sources
% ('SourceSamples') and the true position of the target ('TruePosition').
% Then, we can also express these values in a relative framework, i.e.
% compute how different the two sources are from each other and how
% different they are from the true target location
% A: Compute the differences between the samples (taking into account again
% that we're on a circle, e.g. 344deg and 8deg are 24deg apart, not 336
SampleDifference=SourceSamples(:,2)-SourceSamples(:,1);
SampleDifference(SampleDifference>180) =SampleDifference(SampleDifference>180) - 360;
SampleDifference(SampleDifference<-180)=SampleDifference(SampleDifference<-180)+ 360;

% B: Compute the average position between the two samples (code looks
% complicated because we're on a circle
index=abs(SourceSamples(:,2)-SourceSamples(:,1))<=180;
SourceAverage(index)=(SourceSamples(index,2)+SourceSamples(index,1))./2;
index=(abs(SourceSamples(:,2)-SourceSamples(:,1))>180);
SourceAverage(index)=360-(abs(SampleDifference(index))./2+min(SourceSamples(index,:),[],2));

% C: Compute how far the average was from the true target position
MeanError=TruePosition-SourceAverage';
MeanError(abs(MeanError)>180)=TruePosition(abs(MeanError)>180) -(360-SourceAverage(abs(MeanError)>180)');

% D: Compute how far each sample is from the true target position
TargSampDiff = repmat(TruePosition,1,2) - SourceSamples;
ind = find(abs(TargSampDiff)>180);
TargSampDiff(ind) = 360-abs(TargSampDiff(ind));

% Store the values
outp.SourceSamples.NonCric= SourceSamplesNonCirc;
outp.SourceSamples.v    = SourceSamples;
outp.SourceSamples.l    = 'Samples from the two sources on each trial';
outp.TargetPosition.v   = TruePosition;
outp.TargetPosition.l   = 'True position of the target on each trial';
outp.SampleDifference.v = SampleDifference;
outp.SampleDifference.l = 'Difference (in deg) between the two samples';
outp.SourceAverage.v    = SourceAverage;
outp.SourceAverage.l    = 'Average of the two sample locations';
outp.AverageTargError.v = MeanError;
outp.AverageTargError.l = 'Difference between the average of the two samples and the true target location';
outp.SourceTargError.v  = TargSampDiff;
outp.SourceTargError.l  = 'Difference between each source and the true target location';
outp.BlockID.v          = BlockID;
outp.TrueSTD.v          = TrueSTD;
outp.TrueSTD.l          = 'True standard deviation of each source on each trial';
outp.settings.SamplingResolution = SamplingResolution;
%%%%
