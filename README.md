**This repository will contain four tutorials on fitting computational models of learning to data from a study of reward-guided decision making, and how this can be applied in the context of analysing neuroimaging data.**

**The tutorials have been developed as part of the WIN graduate programme, by Laurence Hunt, Nils Kolling, Miriam Klein-Flugge and Jacqueline Scholl.** The 2019 course is being run by Laurence and Nils.

In the first tutorial (developed by Laurence), we introduce a simple reinforcement learning model for a task that is based around the paradigm of Behrens et al., 2007, but it has an additional (between-subjects) stress manipulation. This paradigm runs in Psychtoolbox, which needs to be downloaded for the paradigm to run properly: see http://psychtoolbox.org. The stress manipulation can be easily switched off, by setting trialvariables.playSound to be 0 for all trials (or by simply muting the sound).

In the second tutorial (developed by Nils), students learn about the basics of reinforcement learning, and the importance of simulating data from a model and parameter recovery from simulated data.

Please let the tutorial authors know if you encounter any unintentional errors. Please also feel free to contact us if you have any questions, although we may not be able to respond to all enquiries.